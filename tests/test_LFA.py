#!/bin/python3
import os
import sys
import shutil
sys.dont_write_bytecode=True
from subprocess import Popen

TestName='LFA' # sub direcotry within TutorialsDir where test scripts are kept

def test_Task1(TutorialsDir,ParsedArgs):
    # Change RunSim and RunDA to False as ShowMesh is automatically set to False
    ParsedArgsNew = ParsedArgs + ['RunSim=False', 'RunDA=False']
    Run = Popen(['VirtualLab','-f','{}/{}/Task1_Run.py'.format(TutorialsDir,TestName),*ParsedArgsNew])
    err = Run.wait()
    assert err==0

def test_Task2(TutorialsDir,ParsedArgs):
    ParsedArgsNew = ParsedArgs + ['RunDA=False']
    Run = Popen(['VirtualLab','-f','{}/{}/Task2_Run.py'.format(TutorialsDir,TestName),*ParsedArgsNew])
    err = Run.wait()
    assert err==0

def test_Task3(TutorialsDir,ParsedArgs):
    ParsedArgsNew = ParsedArgs + ['RunDA=False']
    Run = Popen(['VirtualLab','-f','{}/{}/Task3_Run.py'.format(TutorialsDir,TestName),*ParsedArgsNew])
    err = Run.wait()
    assert err==0

def test_Task4(TutorialsDir,ParsedArgs):
    ParsedArgsNew = ParsedArgs + ['RunDA=False']
    Run = Popen(['VirtualLab','-f','{}/{}/Task4_Run.py'.format(TutorialsDir,TestName),*ParsedArgsNew])
    err = Run.wait()
    assert err==0

def test_Task5(TutorialsDir,ParsedArgs):
    ParsedArgsNew = ParsedArgs + ['RunDA=False']
    Run = Popen(['VirtualLab','-f','{}/{}/Task5_Run.py'.format(TutorialsDir,TestName),*ParsedArgsNew])
    err = Run.wait()
    assert err==0
