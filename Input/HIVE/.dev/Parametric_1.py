from types import SimpleNamespace as Namespace


Sim = Namespace()
Sim.Name = ['Thresh09', 'Thresh099', 'Thresh0999', 'Thresh09999','Thresh1']
Sim.EMThreshold = [0.9,0.99,0.999,0.9999,1]
#Sim.Run = ['Y','N','N','N','N']

