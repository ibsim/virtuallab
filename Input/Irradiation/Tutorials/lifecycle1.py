from types import SimpleNamespace as Namespace


lifecycle = Namespace()
lifecycle.Name = 'unirradiated_day0'
lifecycle.File='lifecycle_post'

DA = Namespace()
DA.Name = 'unirradiated_day0'
DA.File='lifecycle_assess'
DA.days='Unirradiated (day 0)'
