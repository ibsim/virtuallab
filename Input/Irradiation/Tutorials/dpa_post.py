from types import SimpleNamespace as Namespace

# Inputs for plotting the dpa distribution across the monoblock

Sim = Namespace()
Sim.Name='unirradiated_day0'
Sim.AsterFile = 'dpa_post'
Sim.Mesh = 'mono'
Sim.dpa=0
