from types import SimpleNamespace as Namespace
GVXR = Namespace()
#############
##  GVXR   ##
#############
#######################

GVXR.Name = ['AMAZE_Al','AMAZE_Cu']
#GVXR.Tube_Voltage = [175]

GVXR.Material_list = [['Al','Al','Al'],['Cu','Cu','Cu']]
