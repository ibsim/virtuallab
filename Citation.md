********************************************************************************
                               VirtualLab - V2.0            
                     A script to run VirtualLab simulations.
********************************************************************************

                 If you use this code in your publication
                  Please site it in the following way:

                 R. Lewis, B.J. Thorpe, Ll.M. Evans (2020) 
              VirtualLab source code (Version !!!) [Source code]. 
            https://gitlab.com/ibsim/virtuallab/-/commit/{commit_id}

            NOTE: You will need to update the version number and url
                        to the commit version you used.
            
********************************************************************************

