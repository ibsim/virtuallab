#usr/bin/env python

from .slsqp_multi import slsqp_multi
from .genetic_algorithm import GA, GA_Parallel
