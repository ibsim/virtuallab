#usr/bin/env python

from .PreProc.Run import main as Mesh
from .Aster.Run import main as Study
